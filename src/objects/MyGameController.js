/*
 * MyGameController
 *
 * Controls the music playback, and other stuff.
 */

 import Settings from '../objects/Settings';

 export default class MyGameController {

  constructor(game, physicsType, randomSelection) {

    this.game = game;
    this.physicsType = physicsType || Phaser.Physics.ARCADE;
    this.randomSelection = randomSelection ||  false;

    switch(this.physicsType) {
      case Phaser.Physics.P2JS : 
      // Setup world physics
      this.game.physics.startSystem(Phaser.Physics.P2JS);
      this.game.physics.p2.world.defaultContactMaterial.friction = 0.3;
      this.game.physics.p2.world.setGlobalStiffness(1e5);
      this.game.physics.p2.setImpactEvents(true);
      // this.game.physics.p2.setPostBroadphaseCallback(this.checkCollision, this);
      break;
      case physicsType == Phaser.Physics.ARCADE : 
      // Setup world physics
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
      break;
    }
    this.setGravity();

    var re = /\d+/g; 
    var str = game.state.current;
    let [currentWorld, currentStage] = str.match(re);
    localStorage['stage_World' + currentWorld] = currentStage;
    this.currentWorld = currentWorld;

    let controls = game.cache.getJSON('worlds_info');
    this.controls = controls['World' + currentWorld];
    let worldCount = this.controls.stages.length;

    this.worldCount = worldCount || 1;
    this.worldStage = currentStage || 1;

    if(currentStage == worldCount) {
      this.nextState = 'Menu';
    } else {
      this.nextState = 'World' + currentWorld + 'Stage' + (parseInt(currentStage) + 1);
    }




    this.board = this.game.add.group();
    this.board.fixedToCamera = true;

    // Default Game elements
    this.route = this.game.add.group();
    this.objects = this.game.add.group();
    this.obstacles = this.game.add.group();
    this.exit = this.game.add.group();
    this.water = this.game.add.group();
    this.movable = this.game.add.group();
    this.fortuneWheel = this.game.add.group();
    this.platforms = this.game.add.group();
    this.staticWheel = this.game.add.group();

    if(this.physicsType == Phaser.Physics.ARCADE) {
      // Default elements physics
      this.route.enableBody = true;
      this.objects.enableBody = true;
      this.obstacles.enableBody = true;
      this.exit.enableBody = true;
      this.water.enableBody = true;
      this.movable.enableBody = true;
      this.platforms.enableBody = true;
      this.staticWheel.enableBody = true;
    }

    this.selectedElement = null; 

    // Setup sounds
    this.fxCoinBounce = this.game.add.audio('sfx.coin_bounce');
    this.fxCoinBounce.volume = 0.4
    this.fxCoinPickup = this.game.add.audio('sfx.coin_pickup');
    this.fxCoinPickup.volume = 0.4
    this.fxHit = this.game.add.audio('sfx.hit');
    this.fxHit.volume = 0.4
    this.fxJump = this.game.add.audio('sfx.jump');
    this.fxJump.volume = 0.4
    this.fxBg = this.game.add.audio('sfx.backtrack');
    this.fxBg.volume = 0.5;
    this.fxBg.loop = true;

    this.fxBg.loopFull();

    // Markers and controllers
    this.gamePaused = false;
    this.wasPaused = false;
    this.ignoreObstacles = false;

    this.count = {};
    this.marker = {};
    this.countCollects = {};
    this.timeCounter = 0;
    this.timer = null ;
    this.finished = false;
    
    this.playSound = true;

    this.setupBoard();
    this.setupModals();

    if(!localStorage[game.state.current]) {
      localStorage[game.state.current] = JSON.stringify({
        showInstructions: true,
      });
    }
    this.stageSettings = JSON.parse(localStorage[game.state.current]);
    if(this.stageSettings.showInstructions === true)
      this.game.time.events.add(Phaser.Timer.SECOND * 0.3, this.showWelcomeMessage, this);

    if(!localStorage.globalSettings) {
      localStorage.globalSettings = JSON.stringify({
        playSound: true
      });
    }
    this.settings = JSON.parse(localStorage.globalSettings);
    if(this.settings.playSound === false) 
      this.fxBg.pause();



    // TODO:
    //   1. Edit constructor parameters accordingly.
    //   2. Adjust object properties.
  }
  setGravity(value = 1000) {
    if(this.physicsType == Phaser.Physics.ARCADE) {
      this.game.physics.arcade.gravity.y = value;
    } else if(this.physicsType == Phaser.Physics.ARCADE) {
      this.game.physics.p2.gravity.y = value;
    }
  }
  selectRandomObject(frame = null) {
    this.player.moves = false;
    this.gamePaused = true;

    let borderColor = 0xf8ff00;
    let colors = [0x74b31f, 0xf8b100, 0xe1001a, 0x005ea8, 0xc4e7f8];


    let radius = frame? frame.width/2  - 5 : (this.game.width>this.game.height? this.game.height : this.game.width)/2 * 0.6;
    let circle = this.game.add.bitmapData(radius*2, radius*2);
    circle.ctx.fillStyle = '#f8ff00';

    // circle.ctx.setFillColor('#FFFFFF', 0,6);
    circle.ctx.beginPath();
    circle.ctx.arc(radius, radius, radius, 0, 2*Math.PI, true); 
    circle.ctx.closePath();
    circle.ctx.fill();
    let wheel;
    if(frame) {
      wheel = this.game.add.sprite(frame.x + radius, frame.y + radius, circle);
    } else {
      wheel = this.game.add.sprite(this.game.width/2, this.game.height/2, circle);
      wheel.fixedToCamera = true;
    }
    wheel.anchor.set(0.5,0.5);
    this.game.physics.arcade.enable(wheel);
    
    let graphics = this.game.add.graphics(0, 0);
    wheel.addChild(graphics);
    graphics.lineStyle(0);

    for (let i = 0; i < this.objects.length; i++) {
      let o = this.objects.getAt(i);
      graphics.beginFill(colors[i], true);
      
      let start = this.game.math.degToRad((360 / this.objects.length) * i);
      let end = this.game.math.degToRad((360 / this.objects.length) * (i + 1));
      
      let angle = (start + end) / 2;
      let s = radius * 2.8 / (this.game.width/2) ;
      let position = {x: Math.cos(angle) * radius * (1.05 - s), y: Math.sin(angle) * radius * (1.05 - s)};
      let object = this.game.add.sprite(position.x, position.y, o.key, o.frame);
      object.angle = (angle + Math.PI/2) * 180 / Math.PI;
      object.anchor.set(0.5, 0.5);
      object.scale.setTo(s, s);
      object.name = o.name;
      wheel.addChild(object);
      // object.body.allowGravity = false;
      // object.body.immovable =  true;

      graphics.arc(0, 0, radius -10, end + this.game.math.degToRad(2), start, true);
      graphics.endFill();
    }
    let arrow = this.game.add.graphics(wheel.x -5, wheel.y - 2);
    arrow.beginFill(0x46fffd, true);
    arrow.moveTo(radius * 1.15,  radius * 0.15);
    arrow.lineTo(radius * 1.15, -radius * 0.15);
    arrow.lineTo(radius * 0.7, 0);
    arrow.lineTo(radius * 1.15,  radius * 0.15);
    arrow.endFill();

    
    wheel.body.allowGravity = false;
    wheel.body.angularVelocity = 300 + 500 * Math.random();
    wheel.body.angularAcceleration = 0;
    wheel.body.angularDrag = 50;

    this.fortuneWheel.add(wheel);
    this.fortuneWheel.add(arrow);
    this.game.world.bringToTop(this.fortuneWheel);
    return wheel;
  }
  update() {
    if(!this.player) return;
    this.platforms.forEach(function(o) {
      if((o.y < this.player.height && o.body.velocity.y < 0) || (o.y > this.game.height - this.player.height && o.body.velocity.y > 0)) {
        o.body.velocity.y *= -1;
      } 
    }, this);
    if(this.finished) {
      if(this.randomSelection) {
        if(typeof this.wheel == 'undefined') {
          this.player.moves = false;
          this.player.gamePaused = true;
          let wheel = this.staticWheel.getAt(0);
          if(this.game.physics.arcade.overlap(this.player, wheel)) {
            this.wheel = this.selectRandomObject(wheel);
          }

        }
        else if(this.selectedElement == null && this.wheel.body.angularVelocity == 0) {
          let angle = this.wheel.angle;
          // angle -= 36;
          angle %= 360;
          angle *= -1;
          if(angle<0) angle += 360;
          let index = Math.floor(angle/72);

          this.selectedElement = this.wheel.getChildAt(1 + index);
          // alert(this.selectedElement.name);

          // this.game.add.tween(this.fortuneWheel).to({
          //   alpha: 0,
          // }, 2000, Phaser.Easing.Exponential.Out, true)
          // .onComplete.add(function() {
          // }, this);
        }

      } else {
        this.exit.alpha = 1;
      }
    }
    if(this.physicsType == Phaser.Physics.ARCADE) {
      // this.game.physics.arcade.collide(this.player, this.gameLayer);
      this.game.physics.arcade.collide(this.player, this.route);
      this.game.physics.arcade.collide(this.player, this.platforms);
      this.game.physics.arcade.collide(this.player, this.movable);
      this.game.physics.arcade.collide(this.route, this.movable);
      this.game.physics.arcade.collide(this.movable, this.movable);
      this.game.physics.arcade.overlap(this.player, this.objects, this.collectObject, null, this);
      if(!this.ignoreObstacles) this.game.physics.arcade.overlap(this.player, this.obstacles, this.gameOver, null, this);
      this.game.physics.arcade.overlap(this.player, this.swirls, this.gameOver, null, this);
      if(this.finished) this.game.physics.arcade.overlap(this.player, this.exit, this.endGame, null, this);
      this.player.isSwimming = this.game.physics.arcade.overlap(this.player, this.water) && (this.player.y > 410);
    }
    if(this.gamePaused === true) {
      this.setGravity(0);
      this.player.moves = false;
      this.wasPaused = this.gamePaused;
      return;
    }
    if(this.wasPaused) {
      this.player.moves = true;
      if(this.player && this.player.isSwimming) {
        this.setGravity(100);
      } else  {
        this.setGravity(1000);
      }
    }

    this.wasPaused = this.gamePaused;
  }
  setupElements() {

    this.exit.alpha = 0;

    // Fix obstacles orientation
    this.obstacles.forEach(function(o) {
      if(typeof o.swap != 'undefined') {
        if(o.swap) {
          o.scale.setTo(-1, 1);
          o.anchor.set(1,0);
        }
      } 
    }, this);
    this.route.forEach(function(o) {
      if(!o.body) return;
      o.body.checkCollision.up = true;
      o.body.checkCollision.left = false;
      o.body.checkCollision.right = false;
      o.body.checkCollision.down = false;
    }, this);
    if(this.physicsType == Phaser.Physics.ARCADE) {
      // Start group physics
      this.route.setAll('body.allowGravity', false);
      this.route.setAll('body.immovable', true);
      this.route.setAll('alpha', 0);
      this.water.setAll('body.allowGravity', false);
      this.water.setAll('body.immovable', true);
      this.water.setAll('alpha', 0);
      this.objects.setAll('body.allowGravity', false);
      this.objects.setAll('body.immovable', true);
      this.obstacles.setAll('body.allowGravity', false);
      this.obstacles.setAll('body.immovable', true);
      this.exit.setAll('body.allowGravity', false);
      this.exit.setAll('body.immovable', true);
      this.platforms.setAll('body.immovable', true);
      this.platforms.setAll('body.allowGravity', false);
      this.staticWheel.setAll('body.immovable', true);
      this.staticWheel.setAll('body.allowGravity', false);
      // this.route.setAll('body.friction', -10);
      // this.movable.setAll('body.friction', -1);
      // this.movable.setAll('body.mass',100);
      // 
      this.platforms.forEach(function(o) {
        o.body.velocity.y = 170 + 40 * Math.random();
      }, this);

    }else if(this.physicsType == Phaser.Physics.P2JS) {
      this.obstacles.forEach(function(o) {
        this.game.physics.p2.enable(o, false);
        o.body.x = o.x + o.width/2;
        o.body.y = o.y + o.height/2;
        // o.body.clearShapes();
        // o.body.loadPolygon('objects_physics', o.name);
      }, this);
      let k=0;
      this.objects.forEach(function(o) {
        this.game.physics.p2.enable(o, false);
        o.body.dynamic = false;
        o.body.x = o.x + o.width/2;
        o.body.y = o.y + o.height/2;
        o.body.isObject = true;
        o.index = k;
        // o.body.clearShapes();
        // o.body.loadPolygon('objects_physics', o.name);
        k++;
      }, this);
      this.route.forEach(function(o) {
        this.game.physics.p2.enable(o, false);
        o.body.static = true;
        o.body.x = o.x + o.width/2;
        o.body.y = o.y + o.height/2;
      }, this);
    }

    // Setup marker
    this.setupMarker();
  }
  setupMarker() {

    this.objects.forEach(function(o) {
      if(o.name == '') {
        o.name = 'default';
      }
      if(typeof this.marker[o.name] == 'undefined') {
        this.marker[o.name] = {
          count: 0,
          collects: 0,
          key: o.key,
          objects: []
        };
      }
      this.marker[o.name].count ++;
    }, this);

    var graphics = this.game.add.graphics(190, 40);

    let i = 0;
    let offsetX = 0;
    for(let key in this.marker) {
      let m = this.marker[key];
      for (let j = 0; j < m.count; j++) {
        graphics.alpha = 0.6;
        graphics.beginFill(0xFFFFFF);
        graphics.drawCircle(30*j + (30*offsetX + 15*i), 0, 25);
        graphics.endFill();

        let o = m.objects[j] = this.board.create(190 + 30*j + (30*offsetX + 15*i), 40, m.key, (key=='default'? j:i));
        o.tint = 0xAAAAAA;
        o.alpha = 0.1;
        o.anchor.set(0.5, 0.5);
        o.width = 15;
        o.height = 15;
      }
      offsetX += m.count;
      i ++;
    }
    graphics.fixedToCamera = true;
    this.game.world.bringToTop(this.board);

  }
  setupPlayer(player) {
    this.player = player;

    var physics = null;
    if(this.physicsType == Phaser.Physics.ARCADE) {
      this.game.physics.arcade.enable(this.player);
      // this.player.body.mass = 0;
      // this.player.body.checkCollision.left = false;
      // this.player.body.checkCollision.right = false;
      // this.player.body.checkCollision.up = false;

    } else if(this.physicsType == Phaser.Physics.P2JS) {
      this.game.physics.p2.enable(this.player);
      this.player.body.damping = 0.5;
      // this.player.body.clearShapes();
      // this.player.body.loadPolygon('character_physics', 'dude_1');
    }

    this.player.body.fixedRotation = true;
    if(this.platforms.countLiving() == 0) {
      this.player.body.collideWorldBounds = true;
    } else {
      this.player.checkWorldBounds = true;
      this.player.events.onOutOfBounds.add(function(o) {
        if(o.y > this.game.height)
          this.game.state.start(this.game.state.current);
      }, this);
    }

    this.game.world.bringToTop(player);
  }
  checkCollision(body1, body2) {
    return false;
    let collision = false;
    if(typeof body2.isObject != 'undefined' && body1.isObject != 'undefined')  {
      if(body2.isObject === true && body1.isObject === true) {
        collision = true;
        if(body1 == this.player.body || body2 == this.player.body) {
          let player;
          let object;
          if(body1 == this.player.body) {
            player = body1.sprite;
            object = body2.sprite;
          } else {
            player = body2.sprite;
            object = body1.sprite;
          }
          //check if is a coin -> this.collectCoin(body2);
          //check if is an obstacle -> do nothing...
          //check if is an enemy -> die...
          //check if is an object -> this.collectObject(body2);
        } 
      }
    }
    return collision;
  }
  
  collectObject(player, object) {
    var m = this.marker[object.name];
    var o = object.name=='default'? m.objects[object.frame] : m.objects[m.collects];
    m.collects ++;

    o.alpha = 1;
    this.game.add.tween(o).from({
      x: object.x - this.game.camera.x + object.width/2,
      y: object.y - this.game.camera.y + object.height/2,
      alpha: 1,
      width: object.width,
      height: object.height

    }, 2000, Phaser.Easing.Bounce.Out, true);

    this.fxCoinPickup.play();
    object.kill();

    if(this.objects.countLiving() == 0) {
      this.finished = true;
      // this.game.add.tween(this.exit).from({
      //   alpha: 0,
      //   width: 0
      // }, 800, Phaser.Easing.Exponential.Out, true);
    }
  }
  gameOver() {
    if(!this.player.body.moves) return;
    this.player.die(function() {
      // console.log('Character just died');
      this.game.state.start(this.game.state.current);
    }, this);
    this.fxHit.play();
  }

  setupBoard() {
    let btnMute = this.board.create(118, 30, 'game_controls', 12);
    btnMute.width = 28;
    btnMute.height = 28;
    btnMute.inputEnabled = true;
    btnMute.input.useHandCursor = true;
    btnMute.events.onInputDown.add(function(btn) {
      if(this.fxBg.isPlaying) {
        this.fxBg.pause();
        this.settings.playSound = false;
      } else {
        this.fxBg.resume();
        this.settings.playSound = true;
      }
      localStorage.globalSettings = JSON.stringify(this.settings);
    }, this);


    let btnInstructions = this.board.create(30, 30, 'game_controls', 4);
    btnInstructions.width = 28;
    btnInstructions.height = 28;
    btnInstructions.inputEnabled = true;
    btnInstructions.input.useHandCursor = true;
    btnInstructions.events.onInputDown.add(this.showWelcomeMessage, this);

    let btnAvatar = this.board.create(68, 18, 'game_controls', 8);
    btnAvatar.width = 40;
    btnAvatar.height = 40;
    // btnAvatar.inputEnabled = true;



    this.logo = this.board.create(5, this.game.height - 5, 'logo');
    this.logo.anchor.set(0, 1);


    this.homeButton = this.board.create(this.game.width - 30, 24, 'game_controls', 0);
    this.homeButton.anchor.set(1, 0);
    this.homeButton.inputEnabled = true;
    this.homeButton.input.useHandCursor = true;
    this.homeButton.events.onInputDown.add(this.goHome, this);

    let startPosition =  {
      x: this.game.width - 12 * this.worldCount,
      y: this.game.height - 20
    };
    this.stageIndicator = this.game.add.graphics(startPosition.x, startPosition.y);
    
    for (let i = 0; i < this.worldCount; i++) {
      if(i == this.worldStage - 1 ) {
        this.stageIndicator.beginFill(0xFF0000);
      } else {
        this.stageIndicator.beginFill(0xFFFFFF);
      }
      this.stageIndicator.drawCircle(12*i, 0, 6);
      this.stageIndicator.endFill();
    }
    this.board.add(this.stageIndicator);
  }
  setupModals() {
    this.game.stage.backgroundColor = 0x00d5e2;
    let _this = this;
    this.game.modal = new gameModal(this.game);
    this.game.modal.createModal({
      type: 'startModal',
      includeBackground: true,
      backgroundColor: this.controls.color || 0x00d5e2,
      backgroundOpacity: 0.62,
      modalCloseOnInput: false,
      fixedToCamera: true,
      itemsArr: [{
        type: 'image',
        content: 'text_frame_w' + this.currentWorld,
        contentScale: 0.80
      },
      {
        type: 'text',
        content: this.controls.title,
        fontFamily: 'obelixpro',  
        fontSize: 28,
        color: "0xef0000",
        offsetY: -110,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },
      {
        type: 'text',
        content: this.controls.stages[this.worldStage - 1].indications || this.controls.stages[this.worldStage - 1].start || this.controls.stages[this.worldStage - 1].info,
        fontFamily: 'didact',
        fontSize: 15,
        color: "0x000000",
        offsetY: -15,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 450,
      },
      {
        type: 'image',
        content: 'play_button',
        contentScale: 0.80,
        offsetY: 120,
        callback: function() {
          _this.hideWelcomeMessage();
        }
      }],

    });
    this.game.modal.createModal({
      type: 'endModal',
      includeBackground: true,
      backgroundColor: this.controls.color || 0x00d5e2,
      backgroundOpacity: 0.62,
      modalCloseOnInput: false,
      fixedToCamera: true,
      itemsArr: [{
        type: 'image',
        content: 'text_frame_w' + this.currentWorld,
        contentScale: 0.80
      },{
        type: 'text',
        content: this.controls.title,
        fontFamily: 'obelixpro',  
        fontSize: 26,
        color: "0xef0000",
        offsetY: -110,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },{
        type: 'text',
        content: this.controls.stages[this.worldStage - 1].end,
        fontFamily: 'didact',  
        fontSize: 20,
        color: "0x000000",
        offsetY: -15,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },{
        type: 'image',
        content: 'play_button',
        contentScale: 0.80,
        offsetY: 120,
        callback: function() {
          _this.nextGame();
        }
      }]
    });

  }
  showWelcomeMessage() {
    this.game.modal.showModal('startModal');
    this.gamePaused = true;
  }
  hideWelcomeMessage() {
    this.game.modal.hideModal('startModal');
    this.gamePaused = false;
    this.stageSettings.showInstructions = false;
    localStorage[this.game.state.current] = JSON.stringify(this.stageSettings);
  }
  goHome() {
    this.game.state.start('Menu', true, false);
  }
  nextGame() {
    this.game.stateTransition.to(this.nextState, true, false);
  }
  endGame(player, object) {
    if(this.randomSelection && this.selectedElement == null) return;
    if(!this.player.body.moves) return;
    var boundsA = player.getBounds();
    var boundsB = object.getBounds();

    var rect = Phaser.Rectangle.intersection(boundsA, boundsB);

    if((rect.width*1.1 >= Math.floor(boundsA.width )|| rect.width*1.1 >= Math.floor(boundsB.width)) && (rect.height*1.1 >= Math.floor(boundsA.height) || rect.height*1.1 >= Math.floor(boundsB.height))) {
      this.player.body.moves = false;
      this.game.add.tween(this.player).to({
        alpha: 0
      }, 1500, Phaser.Easing.Bounce.Out, true)
      .onComplete.add(function() {
        if(this.nextState == 'Menu') {
          localStorage.removeItem('stage_World' + this.currentWorld);
          this.game.modal.showModal('endModal');
          this.gamePaused = true;
        } else {
          this.game.modal.showModal('endModal');
          this.gamePaused = true;
          // this.nextGame();
        }
      }, this);

      this.fxHit.play();
    }
  }
  shutdown() {
    this.fxBg.destroy();
  }
  resize() {
    this.homeButton.x = this.game.width - 30;
    this.stageIndicator.x = this.game.width - 12 * this.worldCount;
    this.stageIndicator.y = this.game.height - 20;
    this.logo.x = 5;
    this.logo.y = this.game.height - 5;
  }
}
