/*
 * Character
 *
 * This is the game's main character. Will be used in all games
 */

export default class Character extends Phaser.Sprite {


  constructor(game, x, y) {
    super(game, x, y, 'character');

    this.fxJump = this.game.add.audio('sfx.jump');

    // TODO:
    //   1. Edit constructor parameters accordingly.
    //   2. Adjust object properties.
    
    this.playerScale = 1;
    this.bodyType = null;
    this.isSwimming = false;
    this.wasSwimming = false;
    this.anchor.set(0.5, 1);
    this.scale.setTo(this.playerScale, this.playerScale);

    this.events.onAddedToGroup.add(this.addedToGroup,this);
    this.events.onKilled.add(this.killed,this);
    this.events.onOutOfBounds.add(this.outOfBounds,this);
    this.events.onRevived.add(this.revived,this);

    this.animations.add('jump', [0, 9, 8, 7], 7, false);
    this.animations.add('walk', [6, 5, 4, 3, 2], 12, true);
    this.animations.add('run', [2, 3], 10, true);
    this.animations.add('down', [6], 10, true);
    this.animations.add('swim', [10, 11, 12], 7, true);
    this.animations.add('killed', [14, 0], 4, true);
    this.animations.add('swim.killed', [15, 10], 4, true);
    this.animations.add('idle', [0], 0, false);
    this.animations.add('swim.idle', [10, 11, 12], 5, true);
    this.animations.play('idle');

    this.gameInputs = {};
    this.gameInputs.left = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.gameInputs.right = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    this.gameInputs.up = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
    this.gameInputs.down = this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    this.gameInputs.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.game.input.keyboard.addKeyCapture([ Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT, Phaser.Keyboard.DOWN, Phaser.Keyboard.UP, Phaser.Keyboard.SPACEBAR ]);

    this.game.camera.follow(this, Phaser.Camera.FOLLOW_LOCKON);

    if(this.body) {
      this.body.friction = 0.1;
    }
  }
  update() {

    if(!this.body) return;
    if(this.body.moves == false) return;
    // state update code
    var canJump = this.checkIfCanJump();
    
    
    
    if(this.isSwimming && this.y > 410) {
      if(!this.wasSwimming) {
        if(this.physicsType == Phaser.Physics.ARCADE) {
          this.game.physics.arcade.gravity.y = 100;
        } else if(this.physicsType == Phaser.Physics.P2JS) {
          this.game.physics.arcade.p2.y = 100;
        }
        if(this.body instanceof Phaser.Physics.Arcade.Body) {
          // this.body.checkCollision.left = true;
          // this.body.checkCollision.right = true;
          // this.body.checkCollision.up = true;
        }
      }
        
      // Prevent player to fall too quickly
      if(this.body.velocity.y > 70) this.body.velocity.y = 70;

      if(this.body instanceof Phaser.Physics.Arcade.Body) {
        this.game.physics.arcade.gravity.y = 100;
        this.body.setSize(94, 50, 6, 27);
      } else if(this.body instanceof Phaser.Physics.P2.Body) {
        this.game.physics.p2.gravity.y = 100;
      }
      this.animations.play('swim');
      if (this.gameInputs.left.isDown)
      {
        this.body.velocity.x = -100;
        this.animations.play('swim');
        this.scale.setTo(-this.playerScale,this.playerScale);
      }
      else if (this.gameInputs.right.isDown)
      {
        this.body.velocity.x = 100;
        this.animations.play('swim');
        this.scale.setTo(this.playerScale,this.playerScale);
      } else {
        this.animations.play('swim.idle');
        this.body.velocity.x = 0;
      }
      if (this.gameInputs.up.isDown) 
      {
        // this.fxSwim.play();      // add swim effect
        this.body.velocity.y = -60;
        this.animations.play('swim');
      }
    } else {
      if(this.wasSwimming) {
        if(this.physicsType == Phaser.Physics.ARCADE) {
          this.game.physics.arcade.gravity.y = 1000;
        } else if(this.physicsType == Phaser.Physics.P2JS) {
          this.game.physics.p2.gravity.y = 1000;
        }
        
        // Fall out from the swimming pool area
        this.fxJump.play();
        this.animations.play('jump');
        this.body.velocity.y = -300;

        if(this.body instanceof Phaser.Physics.Arcade.Body) { 
          // this.body.checkCollision.left = false;
          // this.body.checkCollision.right = false;
          // this.body.checkCollision.up = false;
        }
      } 
      
      if(this.body instanceof Phaser.Physics.Arcade.Body) {
        this.game.physics.arcade.gravity.y = 1000;
        this.body.setSize(50, 94, 28, 11);
      } else if(this.body instanceof Phaser.Physics.P2.Body) {
        this.game.physics.p2.y = 1000;
      }

      if (this.gameInputs.left.isDown)
      {
        this.body.velocity.x = -150;
        if(canJump) this.animations.play('walk');
        this.scale.setTo(-this.playerScale,this.playerScale);
      }
      else if (this.gameInputs.right.isDown)
      {
        this.body.velocity.x = 150;
        if(canJump) this.animations.play('walk');
        this.scale.setTo(this.playerScale,this.playerScale);
      }
      else {
        if(canJump) this.animations.play('idle');
        this.body.velocity.x = 0;
      }
      if(this.gameInputs.spacebar.isDown) {
        this.body.velocity.x *= 1.5;
      }
      if (canJump && this.gameInputs.up.isDown) 
      {
        this.fxJump.play();
        this.animations.play('jump');
        this.body.velocity.y = -600;
      }
    }

    this.wasSwimming = this.isSwimming;
  }

  checkIfCanJump() {
    if(!this.body) return false;
    if(this.body instanceof Phaser.Physics.P2.Body) {
      var yAxis = p2.vec2.fromValues(0, 1);
      var result = false;

      for (var i = 0; i < this.game.physics.p2.world.narrowphase.contactEquations.length; i++)
      {
        var c = this.game.physics.p2.world.narrowphase.contactEquations[i];

        if (c.bodyA === this.body.data || c.bodyB === this.body.data)
        {
          var d = p2.vec2.dot(c.normalA, yAxis); // Normal dot Y-axis
          if (c.bodyA === this.body.data) d *= -1;
          if (d > 0.5) result = true;
        }
      }

      return result;
    } else if(this.body instanceof Phaser.Physics.Arcade.Body) {
      return this.body.touching.down;
    }
  }
  resize() {

  }
  killed() {

  }
  outOfBounds() {

  }
  addedToGroup() {

  }
  revived() {

  }
  die(callback, controller) {
    if(!this.body.moves) return false;
    if(this.isSwimming) {
      this.animations.play('swim.killed');
    } else  {
      this.animations.play('killed');
    }
    this.body.moves = false;
    var start = this.game.add.tween(this).to({
      x: this.x,
      y: this.y - 80
    }, 500, Phaser.Easing.Exponential.Out);
    var end = this.game.add.tween(this).to({
      x: this.x,
      y:this.game.height + this.height
    }, 500, Phaser.Easing.Exponential.In);
    start.chain(end);
    end.onComplete.add(callback, controller);
    start.start();
  }
}
