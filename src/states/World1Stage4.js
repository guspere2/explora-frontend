/*
 * World1Stage4 state
 *
 * Juego de dinosaurios
 */

import GameMap from '../objects/GameMap';
import Character from '../objects/Character';
import MyGameController from '../objects/MyGameController';

export default class World1Stage4 extends Phaser.State {

  create() {
    // Load Game Map
    this.map = new GameMap(this.game);

    this.gameController = new MyGameController(this.game);
    this.loadGameElements();
    this.gameController.setupElements();
    

    var player = this.player = this.add.existing(new Character(this.game, 10, this.game.height - 100));
    this.gameController.setupPlayer(player);
    
    this.resize();
  }

  loadGameElements() {
    // Declare Custom Game Elements
    let background = this.game.add.group();
    let midground = this.game.add.group();
    let foreground = this.game.add.group();

    let dinos = this.game.add.group();
    let swirls = this.swirls = this.game.add.group();
    swirls.enableBody = true;

    // Load map elements
    this.map.createFromObjects('Background', 5, 'bg_w1s4', null, true, false, background);
    this.map.createFromObjects('Midground', 6, 'mg_w1s4', null, true, false, midground);
    this.map.createFromObjects('Foreground', 7, 'fg_w1s4', null, true, false, foreground);
    this.map.createFromObjects('Foreground', 16, 'dino', 0, true, false, dinos);
    this.map.createFromObjects('Obstaculos', 12, 'swirl', 0, true, false, swirls);
    this.map.createFromObjects('Obstaculos', 8, 'clear', 0, true, false, this.gameController.obstacles);
    this.map.createFromObjects('Exit', 17, 'exit', 0, true, false, this.gameController.exit);
    this.map.createFromObjects('Colisiones', 8, 'clear', 0, true, false, this.gameController.route);
    for (var i = 0; i < 3; i++) {
      // this.map.createFromObjects('Objects', 9 + i, 'objects_w1', i, true, false, this.gameController.objects);
      this.map.createFromObjects('Objects', 9 + i, 'frogs', i, true, false, this.gameController.objects);
    }
    midground.fixedToCamera = true;

    dinos.callAll('animations.add', 'animations', 'attack', [0, 1, 2, 1], 6, true);
    dinos.callAll('animations.play', 'animations', 'attack');
    swirls.callAll('animations.add', 'animations', 'spin', [0, 1], 3, true);
    swirls.callAll('animations.play', 'animations', 'spin');
    this.gameController.objects.callAll('scale.setTo', 'scale', 0.8, 0.8);

    swirls.forEach(function(o) {
      // this.gameController.obstacles.add(o);
    }, this);
    dinos.forEach(function(o) {
      if(typeof o.swap != 'undefined') {
        if(o.swap) {
          o.scale.setTo(-1, 1);
          o.anchor.set(1,0);
        }
      } 
      if(o.rotation != 0) {
        o.anchor.setTo(0.5,0);
      }
    });
    swirls.setAll('body.allowGravity', false);
    swirls.setAll('body.immovable', true);
    swirls.callAll('body.setCircle', 'body', 30, 5, 5);

    // this.game.world.bringToTop(this.background);
    this.game.world.bringToTop(dinos);
    this.game.world.bringToTop(this.gameController.objects);
    this.game.world.bringToTop(this.gameController.obstacles);
    this.game.world.bringToTop(swirls);
    this.game.world.bringToTop(foreground);
    this.game.world.bringToTop(this.gameController.exit);
  }
  render() {
    // this.game.debug.body(this.player);
    // this.gameController.platforms.forEachAlive(this.renderGroup, this);
    // this.gameController.obstacles.forEachAlive(this.renderGroup, this);
    // this.swirls.forEachAlive(this.renderGroup, this);
    // this.gameController.objects.forEachAlive(this.renderGroup, this);
    // this.gameController.route.forEachAlive(this.renderGroup, this);
    if(this.gameController.wheel) {
      // this.game.debug.body(this.gameController.wheel);
      // this.game.debug.body(this.gameController.wheel.getChildAt(0));
      // this.game.debug.body(this.gameController.wheel.getChildAt(1));
      // this.game.debug.body(this.gameController.wheel.getChildAt(2));

    }
  }
  renderGroup(member) {
    this.game.debug.body(member);
  }
  update() {
    this.gameController.update();
    this.game.physics.arcade.overlap(this.player, this.swirls, this.gameController.gameOver, null, this.gameController);
  }
  shutdown() {
    this.gameController.shutdown();
  }
  resize() {
    this.gameController.resize();
  }

}
