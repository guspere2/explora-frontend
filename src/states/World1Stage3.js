/*
 * World1Stage3 state
 *
 * Juego Ahorcado
 */

import GameMap from '../objects/GameMap';
import Character from '../objects/Character';
import MyGameController from '../objects/MyGameController';
import Keyboard from '../objects/Keyboard';

export default class World1Stage3 extends Phaser.State {

  constructor() {
    super();
    this.map = null;
    this.isInit = false;
    this.words = [
      'IDENTIDAD',
      'SENSIBILIDAD SOCIAL',
      'INQUIETUD INTELECTUAL',
      'CREATIVIDAD'
    ];
    this.asserts = 0;
    this.errors = 0;
  }

  create() {
    // Load Game Map
    this.map = new GameMap(this.game, 'World1Stage2Map');


    this.gameController = new MyGameController(this.game);
    this.loadGameElements();
    this.gameController.setupElements();

    
    let player = this.add.existing(new Character(this.game, 1700, this.game.height - 25));
    this.gameController.setupPlayer(player);
    this.gameController.ignoreObstacles = true;


    // TODO: Stub
    this.game.time.events.add(Phaser.Timer.SECOND * 1, this.setupAhorcado, this);
    this.resize();
  }
  loadGameElements() {
    // Declare Custom Game Elements
    this.background = this.game.add.group();
    this.palabras = this.gameController.obstacles;
    this.completar = this.game.add.group();
    let frames = this.game.add.group();
    this.currentWord = this.game.add.group();
    this.currentSpaces = this.game.add.group();

    // Load Controller elements
    this.map.createFromObjects('Route', 1, 'clear', null, true, false, this.gameController.route);
    // Current game Elements
    this.map.createFromObjects('Background', 4, 'bg_mountain', null, true, false, this.background);
    this.map.createFromObjects('Route', 5, 'fg_mountain', null, true, false, this.background);
    this.map.createFromObjects('Frame', 2, 'frame_completar', null, true, false, frames);
    this.map.createFromObjects('Frame', 3, 'frame_ahorcado', null, true, false, frames);
    this.map.createFromObjects('Frame', 18, 'completar', null, true, false, frames);
    this.map.createFromObjects('Cuestionario', 1, 'clear', null, true, false, this.completar);
    this.map.createFromObjects('Exit', 19, 'exit', null, true, false, this.gameController.exit);

    for (var i = 0; i < 12; i++) {
      this.map.createFromObjects('Palabras', 6 + i, 'palabras', i, true, false,this.palabras);
    }
    this.palabras.alpha = 0;

    this.game.world.bringToTop(this.background);
    this.game.world.bringToTop(this.palabras);
    this.game.world.bringToTop(frames);
    this.game.world.bringToTop(this.gameController.exit);

    this.frame= frames.getAt(1);


    let pos = this.gamePosition = {
      x: this.frame.x + 10,
      y: this.frame.y + 120
    };
    let _this = this;
    let keyboard = this.keyboard = new Keyboard(this.game, function(l) {
      _this.keyPressed(l);
    }, pos.x + 200, pos.y + 100, 0.4*this.frame.height, 0.55*this.frame.width);
    this.game.add.existing(keyboard);

    this.ranita = this.game.add.group();
    this.ranitaCount = 6;
    for (var i = this.ranitaCount -1; i >= 0; i--) {
      this.ranita.create(pos.x, pos.y, 'ranita', i);
    }
    this.game.world.bringToTop(this.ranita);
    this.ranita.callAll('scale.setTo', 'scale', 0.37, 0.37);
  }
  setupAhorcado() {
    this.finishedAhorcado = false;
    this.startedAhorcado = false;

    let exit = this.gameController.exit.getAt(1);
    let start = this.frame.x - 200; 
    let width = exit.x + exit.width - start + 200;
    this.game.world.setBounds(
      start,
      0,
      width,
      this.game.world.height
      );

    this.nextWord();
  }
  nextWord() {
    this.keyboard.refresh();

    this.currentWord.removeAll(true, true);
    this.currentSpaces.removeAll(true, true);

    var p = this.words.pop();
    if(!p) {
      this.endGame();
      return;
    }
    this.asserts = 0;
    this.errors = 0;
    this.ranita.setAll('alpha', 0);


    var width = 28 // example;
    var height = 40 // example;
    var bmd = this.game.add.bitmapData(width, height);
    bmd.ctx.beginPath();
    bmd.ctx.rect(0,0,width,height);
    bmd.ctx.fillStyle = '#da173a';
    bmd.ctx.fill();

    var res = p.split(" ");
    for (var k = 0; k < res.length; k++) {
      var w = res[k];
      var startPoint = {
        x: this.keyboard.rect.x + this.keyboard.rect.width - (w.length * 31) / 2 - 10,
        y: this.keyboard.rect.y - 120 + 50*k - 25*(res.length - 1)
      }
      this.asserts = 0;

      for (var i = 0; i < w.length; i++) {
        var c = w.charAt(i);

        var rectangle = this.game.add.sprite(startPoint.x, startPoint.y, bmd);
        rectangle.anchor.set(0.5, 0.5);
        var letter = this.game.add.text(startPoint.x, startPoint.y + 6, c, {
          font: '28px obelixpro',
          fill: '#FFFFFF',
          align: 'center'
        });
        letter.anchor.set(0.5,0.5);
        letter.letter = c;
        letter.alpha = 0;
        startPoint.x += 31;
        
        this.currentWord.add(rectangle);
        this.currentWord.add(letter);
      }
    }
    this.game.world.bringToTop(this.currentWord);
  }
  update() {
    // TODO: Stub
    this.gameController.update();
  }
  endGame() {
    this.game.add.tween(this.keyboard).to({
      alpha: 0
    }, 800, Phaser.Easing.Exponential.Out, true);
    this.ranita.setAll('alpha', 1);
    this.ranita.alpha = 0;
    this.game.add.tween(this.ranita).to({
      alpha: 1,
      x: this.game.world.x - this.frame.width/2,
      y: this.frame.y - this.frame.height/2,
    }, 2000, Phaser.Easing.Exponential.Out, true);
    this.gameController.finished = true;
  }
  resize() {
    this.gameController.resize();
  }
  keyPressed(l) {
    let match = 0;
    this.currentWord.forEach(function(o) {
      if(o.letter == l) {
        console.log('Did match');
        match ++;
        o.alpha = 1;
      }
    }, this);
    if(match==0) {
      this.ranita.getAt(this.errors).alpha = 1;
      this.errors ++;
      if(this.errors == this.ranitaCount) {
        this.gameOver();
      }
    } else {
      this.asserts += match;
    }
    if(this.asserts >= this.currentWord.length/2) {
      this.game.time.events.add(Phaser.Timer.SECOND * 0.8, this.nextWord, this);
    }
  }
  gameOver() {
    this.game.state.start(this.game.state.current);
  }
}
