/*
 * World1Stage2 state
 *
 * Crossword Puzzle Game
 */

 import GameMap from '../objects/GameMap';
 import Character from '../objects/Character';
 import MyGameController from '../objects/MyGameController';

export default class World1Stage2 extends Phaser.State {

  constructor() {
    super();
    this.map = null;

    this.isInit = false;
  }

  create() {
    // Load Game Map
    this.map = new GameMap(this.game);

    this.gameController = new MyGameController(this.game);

    // Setup Groups
    this.palabras = this.gameController.obstacles;
    this.completar = this.game.add.group();

    this.loadGameElements();

    this.gameController.setupElements();

    
    var player = this.add.existing(new Character(this.game, 440, this.game.height - 25));
    this.gameController.setupPlayer(player);
    this.gameController.ignoreObstacles = true;


    // TODO: Stub
    this.game.time.events.add(Phaser.Timer.SECOND * 1, this.setupCompletar, this);
    
    this.resize();


  }
  loadGameElements() {
    // Declare Custom Game Elements
    this.background = this.game.add.group();
    let frames = this.game.add.group();

    // Load Controller elements
    this.map.createFromObjects('Route', 1, 'clear', null, true, false, this.gameController.route);
    // Current game Elements
    this.map.createFromObjects('Background', 4, 'bg_mountain', null, true, false, this.background);
    this.map.createFromObjects('Route', 5, 'fg_mountain', null, true, false, this.background);
    this.map.createFromObjects('Frame', 2, 'frame_completar', null, true, false, frames);
    this.map.createFromObjects('Frame', 3, 'frame_ahorcado', null, true, false, frames);
    this.map.createFromObjects('Frame', 18, 'completar', null, true, false, frames);
    this.map.createFromObjects('Cuestionario', 1, 'clear', null, true, false, this.completar);
    this.map.createFromObjects('Exit', 19, 'exit', null, true, false, this.gameController.exit);

    for (var i = 0; i < 12; i++) {
      this.map.createFromObjects('Palabras', 6 + i, 'palabras', i, true, false,this.palabras);
    }
    this.gameController.obstacles.alpha = 0;


    this.game.world.bringToTop(this.background);
    this.game.world.bringToTop(this.palabras);
    this.game.world.bringToTop(frames);
    this.game.world.bringToTop(this.gameController.exit);

    this.frame = frames.getAt(0);
  }

  setupCompletar() {
    if(this.isInit) return;
    this.isInit = true;
    this.game.add.tween(this.palabras).to({
      alpha:1
    }, 2000, Phaser.Easing.Bounce.Out, true);
    let text = this.game.add.text(this.frame.x + this.frame.width/2, this.frame.y + 48, 'Mision Explora', {
      font: 'bold 30px obelixpro',
      fill: '#FFFFFF',
      align: 'center',
    });
    text.anchor.set(0.5, 0.5);

    let exit = this.gameController.exit.getAt(0);
    let start = this.frame.x - 200; 
    let width = exit.x + exit.width - start + 200;
    this.game.world.setBounds(
      start,
      0,
      width,
      this.game.world.height
      );

    this.currentWord = null;
    this.currentTween = null;
    this.palabras.forEachAlive(function(p) {
      p.inputEnabled = true;
      p.events.onInputDown.add(this.selectWord, this);
    }, this);
    
    this.completar.forEachAlive(function(p) {
      p.inputEnabled = true;
      p.events.onInputDown.add(this.matchWord, this);
    }, this);

  }
  selectWord(word) {
    if(this.currentTween)
      this.currentTween.stop();
    if(this.currentWord && this.currentWord.name == word.name) {
      this.currentWord = null;
      return;
    }
    var w = word.width;
    var h = word.height;
    this.currentWord = word;
    let start = this.currentTween = this.game.add.tween(word)
      .to({
        width: w * 1.2,
        height: h * 1.2,
        x: word.x - w * 0.1,
        y: word.y - h * 0.1,
        alpha: 0.8
      }, 800, Phaser.Easing.Bounce.Out);
    let stop = this.game.add.tween(word)
      .to({
        width: w,
        height: h,
        alpha: 1
      }, 800, Phaser.Easing.Bounce.In);
    start.chain(stop).loop(true).start();

  }
  matchWord(match) {
    if(!this.currentWord) return;
    if(this.currentWord.name == match.name) {
      // There is a match.
      var text = this.game.add.text(match.x + match.width/2, match.y + (match.height/2) + 2, match.name, {
        font: 'bold 15px sans-serif',
        fill: '#FFFFFF',
        align: 'center',
      });
      text.anchor.set(0.5, 0.5);
      this.currentWord.kill();
      match.kill();
    } else {
      // if(this.currentTween)
      //   this.currentTween.stop();
      // this.currentWord = null;
    }
    if(this.palabras.countLiving() == 0) {
      // you finished the game.
      this.endGame();
    }
  }
  update() {
    // TODO: Stub
    this.gameController.update();
  }
  endGame() {
    this.gameController.finished = true;
  }
  resize() {
    this.gameController.resize();
  }

}
