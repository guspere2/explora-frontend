/*
 * World1Stage1 state
 *
 * Water Game
 */
 import GameMap from '../objects/GameMap';
 import Character from '../objects/Character';
 import MyGameController from '../objects/MyGameController';

export default class World1Stage1 extends Phaser.State {

  create() {

    // Load Game Map
    this.map = new GameMap(this.game);

    this.gameController = new MyGameController(this.game);


    this.loadGameElements();
    

    this.gameController.setupElements();




    
    var player = this.add.existing(new Character(this.game, 10, 300));
    this.gameController.setupPlayer(player);


    // TODO: Stub
    
    this.resize();
  }

  loadGameElements() {

    // Declare Custom Game Elements
    this.background = this.game.add.group();
    this.burbujas = this.game.add.group();


    // Load Controller elements
    this.map.createFromObjects('Route', 2, 'clear', null, true, false, this.gameController.route);
    this.map.createFromObjects('Obstacles', 3, 'pez_1', 0, true, false, this.gameController.obstacles);
    this.map.createFromObjects('Obstacles', 5,  'pez_2', 0, true, false, this.gameController.obstacles);
    this.map.createFromObjects('Obstacles', 20, 'pez_gordo', 0, true, false, this.gameController.obstacles);
    this.map.createFromObjects('Obstacles', 19, 'exit_agua', null, true, false, this.gameController.exit);
    for (var i = 0; i < 12; i++) {
      this.map.createFromObjects('Objects', 7 + i, 'palabras', i, true, false,this.gameController.objects);
    }
    // Current game Elements
    this.map.createFromObjects('Background', 1, 'background', null, true, false, this.background);
    this.map.createFromObjects('SwimArea', 2, 'clear', null, true, false, this.gameController.water);
    this.map.createFromObjects('Obstacles', 22, 'burbujas', 0, true, false, this.burbujas);
    
    // Setup objects Animations
    this.gameController.obstacles.callAll('animations.add', 'animations', 'swim', [0, 1], 6, true);
    this.gameController.obstacles.callAll('animations.play', 'animations', 'swim'); 
    this.burbujas.callAll('animations.add', 'animations', 'bubble', [0, 1, 2], 8, true);
    this.burbujas.callAll('animations.play', 'animations', 'bubble');

    console.log(this.map);

    // this.game.world.bringToTop(this.background);
    this.game.world.bringToTop(this.gameController.obstacles);
    this.game.world.bringToTop(this.gameController.objects);
    this.game.world.bringToTop(this.burbujas);
    this.game.world.bringToTop(this.gameController.exit);

  }
  update() {
    this.gameController.update();
  }
  shutdown() {
    this.gameController.shutdown();
  }
  resize() {
    this.gameController.resize();
  }
}
