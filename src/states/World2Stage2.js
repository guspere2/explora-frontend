/*
 * World2Stage1 state
 *
 * Pasillo del tiempo
 */

 import GameMap from '../objects/GameMap';
 import Character from '../objects/Character';
 import MyGameController from '../objects/MyGameController';

export default class World2Stage1 extends Phaser.State {

  create() {
    // Load Game Map
    this.map = new GameMap(this.game);
    this.finished = false;

    this.gameController = new MyGameController(this.game, null, true);
    this.loadGameElements();
    this.gameController.setupElements();
    this.gameController.route.forEach(function(o) {
      o.body.checkCollision.left = true;
      o.body.checkCollision.right = true;
    });

    var player = this.player = this.add.existing(new Character(this.game, 10, this.game.height - 130));
    this.gameController.setupPlayer(player);

    this.resize();

    this.texts = {
      'rubik': 'Diseñar, formular e implementar estrategias innovadoras en educación.',
      'microphone': 'Generar contenidos convocantes e impactantes para la generación de valor.',
      'brain': 'Estimular la creatividad ciudadana y el desarrollo de proyectos innovadores.',
      'planet': 'Generar contenidos convocantes e impactantes para la generación de valor.',
      'turtle': 'Diseñar, formular e implementar estrategias innovadoras en educación.',
    };

  }

  update() {
    // TODO: Stub
       this.gameController.update();

       if(!this.finished && this.gameController.selectedElement !== null) {
         this.finished = true;
         let frame = this.gameController.staticWheel.getAt(1);
         
         let o = this.game.add.sprite(frame.x + frame.width/2, frame.y + 110, this.gameController.selectedElement.key, this.gameController.selectedElement.frame);
         o.anchor.set(0.5,0.5);
         o.name = this.gameController.selectedElement.name;
         let title = this.welcomeTitle = this.game.add.text(o.x, o.y - o.height / 2 - 28, 'Nuestros objetivos', {
           font: '24px didact',
           wordWrap: true,
           wordWrapWidth: frame.width*0.8,
           align: 'center',
           fill: '#000000'
         });
         title.anchor.set(0.5, 0.5);
         let text = this.game.add.text(o.x, o.y + o.height / 2, this.texts[o.name], {
           font: '28px didact',
           wordWrap: true,
           wordWrapWidth: frame.width*0.7,
           align: 'center',
           fill: '#000000'
         });
         text.anchor.set(0.5, 0);

         this.game.time.events.add(Phaser.Timer.SECOND * 3, function() {
           this.gameController.exit.alpha = 1;
          }, this);

         
       }
       this.game.physics.arcade.collide(this.gameController.route, this.gameController.objects);
       this.game.physics.arcade.collide(this.gameController.platforms, this.gameController.objects);

  }
  resize() {
    this.gameController.resize();
  }
  loadGameElements() {
    // Setup Groups
    
    var exit = this.game.add.group();
    var start = this.game.add.group();

    this.map.createFromObjects('Route', 2, 'bg_w2s2', null, true, false);
    this.map.createFromObjects('Platforms', 1, 'clear', null, true, false, this.gameController.route);
    this.map.createFromObjects('Start', 1, 'clear', null, true, false, start);
    this.map.createFromObjects('Exit', 9, 'exit_door', null, true, false, this.gameController.exit);
    this.map.createFromObjects('Exit', 11, 'fortune_wheel', null, true, false, this.gameController.staticWheel);
    this.map.createFromObjects('Exit', 10, 'end_frame_w2', null, true, false, this.gameController.staticWheel);
    this.map.createFromObjects('Obstacles', 3, 'box', null, true, false, this.gameController.movable);
    for (var i = 0; i < 5; i++) {
      this.map.createFromObjects('Objects', 4 + i, 'objects_w2', i, true, false, this.gameController.objects);
    }

    this.game.world.bringToTop(this.gameController.route);
    this.game.world.bringToTop(this.gameController.obstacles);
    this.game.world.bringToTop(this.gameController.staticWheel);
    this.game.world.bringToTop(this.gameController.exit);
    this.game.world.bringToTop(this.gameController.movable);
    this.game.world.bringToTop(this.gameController.objects);

  }
  render() {
    // this.game.debug.body(this.player);
    // this.gameController.obstacles.forEachAlive(this.renderGroup, this);
    // this.gameController.objects.forEachAlive(this.renderGroup, this);
    // this.gameController.route.forEachAlive(this.renderGroup, this);
  }
  renderGroup(member) {
    this.game.debug.body(member);
  }

}
