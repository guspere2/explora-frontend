/*
 * World4Stage1 state
 *
 * El Universo
 */

 export default class World4Stage1 extends Phaser.State {

  create() {
    // TODO: Stub
    
    this.game.stage.backgroundColor = '#041935'; 
    this.game.physics.startSystem(Phaser.Physics.ARCADE);


    var re = /\d+/g; 
    var str = this.game.state.current;
    let [currentWorld, currentStage] = str.match(re);
    localStorage['stage_World' + currentWorld] = currentStage;
    this.currentWorld = currentWorld;
    this.worldStage = 1;

    let controls = this.game.cache.getJSON('worlds_info');
    this.controls = controls['World' + currentWorld];
    let worldCount = this.controls.stages.length;

    this.game.world.setBounds(0, 0, 2200, 2200);
    //  The scrolling starfield background
    this.starfield = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'starfield');

    let sun = this.sun = this.game.add.sprite(this.game.world.width/2, this.game.world.height/2, 'planets', 0);
    this.game.physics.arcade.enable(sun);
    sun.body.immovable = true;
    sun.body.allowGravity = false;
    sun.anchor.set(0.5,0.5);
    // sun.scale.setTo(2, 2);

    this.planets = this.game.add.group();
    this.planets.enableBody = true;

    let data = [{
      name: 'sun'
    }, {
      name:'E.XP1 (Experiencias)', 
      description: 'Es el primero en nuestro sistema solar. +'+
      'Recibe su nombre a las EXPERIENCIAS que realizamos a lo largo y ancho de todos nuestros servicios. ' +
      'Se trata del planeta que ofrece un mayor brillo en: escuela explora / explora eventos / servicios '+
      'comerciales complementarios y además cubre también las estrategias: \n'+
      ' - apropiación\n - ste@m / escenarios de aprendizaje / políticos\n - apropiación.\nÉste puede verse desde cualquier punto del Universo Explora.'
    },{
      name: 'earth',
    },{
      name: 'CAN 4.3 (Canales)',
      description: 'Está ubicado en el centro de nuestro universo.  Viajar a él es más simple de lo que te imaginas. \n'+
      'Es llamado así por los CANALES que utilizamos en todas nuestras estrategias como organización. Aunque en apariencia \n'+
      'podría parecer un planeta simple, no lo es. Sus aspectos principales son: Parque Explora - Acuario - Planetario / Proyectos / Asesorias. \n'+
      'Además contiene elementos como: Redes / Transmedia / Estrategias intinerantes / Cursos / Tienda / Zona de alimentación y servicios del \n'+
      'Parque. Este puede verse con un simple telescopio.'
    },{
      name: 'COM.d.ES (Comunidades)',
      description: 'es el planeta más grande. Su nombre proviene de nuestro fin las COMUNIDADES.Debido a su posición orbital es muy importante para nuestro sistema. '+
      'Los aspectos más característicos son: Turistico - Niñez - Familia / Comunidades de Práctica / Pares y Gremios. Sobre él se extiende una extensa capa de: '+
      'Públicos Especializados / Juventud / Comunidad Educativa / Empresas Privadas / Cooperante Financieros / Entidades Públicas y Territoriales.'
    }];

    let step = (this.game.world.width / 2) * 0.9 / 4;
    for (let i = 1; i < 5; i++) {
      let p = this.planets.create(0 , 0, 'planets', i);
      p.anchor.set(0.5, 0.5);
      p.pivot.x = -step * i;
      p.pivot.y = 0;

      p.name = data[i].name;
      if(typeof data[i].description != 'undefined') p.description = data[i].description;

      this.game.physics.arcade.enable(p);
      p.translationSpeed = (Math.random() - 0.5) / 180;
      let rotationSpeed = (Math.random() - 0.5) * 50;
      p.body.angularVelocity = rotationSpeed / i;
      p.body.angularAcceleration = 0;
      // p.body.angularDrag = 50;
      p.body.immovable = true;
      p.body.allowGravity = false;
      sun.addChild(p);
    }

    let spaceStation = this.spaceStation = this.game.add.sprite(this.game.world.width - 300, 300, 'space_station');
    this.spaceStation.anchor.set(1, 0);
    this.game.physics.arcade.enable(spaceStation);

    this.rulerTop = this.game.add.sprite(this.game.width / 2 , 5, 'ruler');
    this.rulerTop.anchor.set(0.5,0);
    this.rulerTop.fixedToCamera = true;
    this.rulerBottom = this.game.add.sprite(this.game.width / 2, this.game.height - 5, 'ruler');
    this.rulerBottom.anchor.set(0.5,0);
    this.rulerBottom.scale.setTo(1,-1);
    this.rulerBottom.fixedToCamera = true;
    
    let player = this.player = this.game.add.sprite(this.game.world.width / 2 + 200, this.game.world.height / 2 - 300, 'moon_module');
    this.game.physics.arcade.enable(this.player);
    player.body.collideWorldBounds = true;
    player.anchor.set(0.5, 0.5);

    let leftGas = player.leftGas = this.game.add.sprite(-70, 0, 'gas');

    let rightGas = player.rightGas = this.game.add.sprite(70, 0, 'gas');
    rightGas.scale.setTo(-1,1);

    let bottomGas1 = player.bottomGas1 = this.game.add.sprite(-28, 64, 'gas');
    bottomGas1.angle = -90;
    bottomGas1.scale.setTo(0.7, 1);
    let bottomGas2 = player.bottomGas2 = this.game.add.sprite(27, 64, 'gas');
    bottomGas2.angle = -90;
    bottomGas2.scale.setTo(0.7, 1);

    let topGas = player.topGas = this.game.add.sprite(-18, - 68, 'gas');
    topGas.angle = 90;
    topGas.scale.setTo(0.8, 1);

    player.addChild(leftGas);
    player.addChild(rightGas);
    player.addChild(bottomGas1);
    player.addChild(bottomGas2);
    player.addChild(topGas);


    this.game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON);


    this.gameInputs = {};
    this.gameInputs.left = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.gameInputs.right = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    this.gameInputs.up = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
    this.gameInputs.down = this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    this.gameInputs.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.game.input.keyboard.addKeyCapture([ Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT, Phaser.Keyboard.DOWN, Phaser.Keyboard.UP, Phaser.Keyboard.SPACEBAR ]);


    


    this.setupModals();
    this.setupBoard();

  }

  setupBoard() {
    if(!localStorage.globalSettings) {
      localStorage.globalSettings = JSON.stringify({
        playSound: true
      });
    }
    this.settings = JSON.parse(localStorage.globalSettings);
    if(!localStorage[this.game.state.current]) {
      localStorage[this.game.state.current] = JSON.stringify({
        showInstructions: true,
      });
    }

    this.logo = this.game.add.image(10, 20, 'logo');
    this.logo.fixedToCamera = true;

    this.homeButton = this.game.add.button(this.game.width - 30, 24, 'game_controls', function(button) {
      this.game.state.start('Menu', true, false);
    }, this, 0, 0, 0, 0);
    this.homeButton.anchor.set(1, 0);
    this.homeButton.fixedToCamera = true;

    this.controlButtons = this.game.add.sprite(this.game.width - 10, this.game.height / 2, 'controller_w4');
    this.controlButtons.fixedToCamera = true;
    this.controlButtons.anchor.set(1, 0.5);
    let showWelcomeButton = this.game.add.button(-this.controlButtons.width / 2, -this.controlButtons.height / 2, 'clear', function() {
      this.showWelcomeMessage();
    }, this);
    showWelcomeButton.anchor.set(0.5,0),
    showWelcomeButton.width = this.controlButtons.width*0.7;
    showWelcomeButton.height = this.controlButtons.width*0.7;

    this.fxBg = this.game.add.audio('sfx.backtrack');
    this.fxBg.volume = 0.5;
    this.fxBg.loop = true;
    this.fxBg.loopFull();
    if(this.settings.playSound === false) 
      this.fxBg.pause();

    let toggleSoundButton = this.game.add.button(-this.controlButtons.width / 2, this.controlButtons.height / 2, 'clear', function() {
      this.toggleSound();
    }, this);
    toggleSoundButton.anchor.set(0.5,1),
    toggleSoundButton.width = this.controlButtons.width * 0.7;
    toggleSoundButton.height = this.controlButtons.width * 0.7;

    this.controlButtons.addChild(toggleSoundButton);
    this.controlButtons.addChild(showWelcomeButton);

    this.stageSettings = JSON.parse(localStorage[this.game.state.current]);
    if(this.stageSettings.showInstructions === true)
      this.game.time.events.add(Phaser.Timer.SECOND * 0.3, this.showWelcomeMessage, this);
  }
  setupModals() {
    this.game.stage.backgroundColor = 0x00d5e2;
    let _this = this;
    this.game.modal = new gameModal(this.game);
    this.game.modal.createModal({
      type: 'startModal',
      includeBackground: true,
      backgroundColor: this.controls.color || 0x00d5e2,
      backgroundOpacity: 0.62,
      modalCloseOnInput: false,
      fixedToCamera: true,
      itemsArr: [{
        type: 'image',
        content: 'text_frame_w2',
        contentScale: 0.80
      },
      {
        type: 'text',
        content: this.controls.title,
        fontFamily: 'obelixpro',  
        fontSize: 28,
        color: "0xef0000",
        offsetY: -110,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },
      {
        type: 'text',
        content: this.controls.stages[this.worldStage - 1].indications || this.controls.stages[this.worldStage - 1].start || this.controls.stages[this.worldStage - 1].info,
        fontFamily: 'didact',
        fontSize: 15,
        color: "0xFFFFFF",
        offsetY: -15,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 450,
      },
      {
        type: 'image',
        content: 'play_button',
        contentScale: 0.80,
        offsetY: 120,
        callback: function() {
          _this.hideWelcomeMessage();
        }
      }],

    });
    this.game.modal.createModal({
      type: 'endModal',
      includeBackground: true,
      backgroundColor: this.controls.color || 0x00d5e2,
      backgroundOpacity: 0.62,
      modalCloseOnInput: false,
      fixedToCamera: true,
      itemsArr: [{
        type: 'image',
        content: 'text_frame_w2',
        contentScale: 0.80
      },{
        type: 'text',
        content: this.controls.title,
        fontFamily: 'obelixpro',  
        fontSize: 26,
        color: "0xef0000",
        offsetY: -110,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },{
        type: 'text',
        content: this.controls.stages[this.worldStage - 1].end,
        fontFamily: 'didact',  
        fontSize: 20,
        color: "0xFFFFFF",
        offsetY: -15,
        align: 'center',
        // offsetX: 0,
        strokeThickness: 0,
        maxWidth: 300,
      },{
        type: 'image',
        content: 'play_button',
        contentScale: 0.80,
        offsetY: 120,
        callback: function() {
          _this.nextGame();
        }
      }]
    });
  }
  showWelcomeMessage() {
    this.game.modal.showModal('startModal');
    this.gamePaused = true;
  }
  hideWelcomeMessage() {
    this.game.modal.hideModal('startModal');
    this.gamePaused = false;
    this.stageSettings.showInstructions = false;
    localStorage[this.game.state.current] = JSON.stringify(this.stageSettings);
  }
  toggleSound() {
    if(this.fxBg.isPlaying) {
      this.fxBg.pause();
      this.settings.playSound = false;
    } else {
      this.fxBg.resume();
      this.settings.playSound = true;
    }
    localStorage.globalSettings = JSON.stringify(this.settings);
  }
  update() {
    // TODO: Stub
    let m = Math.random() * 0.4;
    this.starfield.tilePosition.x += m;
    this.starfield.tilePosition.y += m;

    // this.planets.forEach(function(p) {
    //   p.rotation += p.translationSpeed;
    // });



    let s = 10;
    if (this.gameInputs.left.isDown)
    {
      this.player.body.velocity.x -= s;
    }
    else if (this.gameInputs.right.isDown)
    {
      this.player.body.velocity.x += s;
    }
    else {
      if (Math.abs(this.player.body.velocity.x) > 0) {
        this.player.body.velocity.x *= 0.995;
        if(Math.abs(this.player.body.velocity.x) < 20)
          this.player.body.velocity.x = 0;
      }
    }
    if (this.gameInputs.up.isDown)
    {
      this.player.body.velocity.y -= s;
    }
    else if (this.gameInputs.down.isDown)
    {
      this.player.body.velocity.y += s;
    }
    else {
      if (Math.abs(this.player.body.velocity.y) > 0) {
        this.player.body.velocity.y *= 0.995;
        if(Math.abs(this.player.body.velocity.y) < 20)
          this.player.body.velocity.y = 0;
      }
    }
    
    if(this.player.body.velocity.x > 150)
      this.player.body.velocity.x = 150;
    if(this.player.body.velocity.x < -150)
      this.player.body.velocity.x = -150;
    if(this.player.body.velocity.y < -150)
      this.player.body.velocity.y = -150;
    if(this.player.body.velocity.y > 150)
      this.player.body.velocity.y = 150;

    if(this.player.body.velocity.x > 0) {
      this.player.leftGas.alpha = 1;
      this.player.rightGas.alpha = 0;
    }
    else if(this.player.body.velocity.x < 0) {
      this.player.leftGas.alpha = 0;
      this.player.rightGas.alpha = 1;
    } else {
      this.player.leftGas.alpha = 0;
      this.player.rightGas.alpha = 0;
    }
    if(this.player.body.velocity.y < 0) {
      this.player.bottomGas1.alpha = 1;
      this.player.bottomGas2.alpha = 1;
      this.player.topGas.alpha = 0;
    } else if(this.player.body.velocity.y > 0) {
      this.player.bottomGas1.alpha = 0;
      this.player.bottomGas2.alpha = 0;
      this.player.topGas.alpha = 1;
    } else {
      this.player.bottomGas1.alpha = 0;
      this.player.bottomGas2.alpha = 0;
      this.player.topGas.alpha = 0;
    }


  }
  destroy() {
    this.fxBg.destroy();
  }
  resize() {
    // this.starfield.width = this.game.width;
    // this.starfield.height = this.game.height;

    this.rulerTop.x = this.game.width / 2;
    this.rulerBottom.x = this.game.width / 2;

    this.homeButton.x = this.game.width - 30;

    this.controlButtons.x = this.game.width - 10;
    this.controlButtons.y = this.game.height / 2;
  }

}
