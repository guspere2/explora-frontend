/*
 * Boot state
 * ==========
 *
 * The first state of the game, responsible for setting up some Phaser
 * features. Adjust the game appearance, number of input pointers, screen
 * orientation handling etc. using this game state.
 */

import assets from '../assets';

export default class Boot extends Phaser.State {

  preload() {
    // Point the Phaser Asset Loader to where your game assets live.
    this.load.path = 'assets/';

    // Initialize physics engines here. Remember that Phaser builds including
    // Arcade Physics have it enabled by default.
    //this.physics.startSystem(Phaser.Physics.P2);

    // Load the graphical assets required to show the splash screen later,
    // using the asset pack data.
    this.load.pack('boot', null, assets);

    var _this = this;
    window.addEventListener("resize", function() {
      _this.gameResized();
    });
  }
  init() {

    // When using 'USER_SCALE' scaling mode, use this method to adjust the
    // scaling factor.
    //this.scale.setUserScale(2);

    // Uncomment the following line to adjust the rendering of the canvas to
    // crisp graphics. Great for pixel-art!
    //Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);

    // Uncomment this line to disable smoothing of textures.
    //this.stage.smoothed = false;

    // Adjust how many pointers Phaser will check for input events.
    this.input.maxPointers = 2;

    //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
    this.stage.disableVisibilityChange = true;

    // Set the alignment of the game canvas within the page.
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;

    //  This tells the game to resize the renderer to match the game dimensions (i.e. 100% browser width / height)
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

    this.gameResized();
  }
  create() {
    // After applying the first adjustments and loading the splash screen
    // assets, move to the next game state.
    this.state.start('Preload');
  }
  gameResized() {
    // console.log('JavaScript Function Run');
    var height = 600;
    var s = height / window.innerHeight;
    var width = window.innerWidth * s;
    if (width < 600) width = 600;
    this.game.width = width;
    this.game.height = height;
    this.game.scale.setGameSize(width, height);
    this.game.state.resize();
  }

}
