/*
 * Menu state
 *
 * The Game's main menu
 */

 export default class Menu extends Phaser.State {

  init() {
    // TODO: Stub
  }

  create() {
    this.isModal = false;
    let x = this.game.width/2 - 76;
    let y = this.game.height/2;

    this.menuGroup = this.game.add.group();
    this.welcomeGroup = this.game.add.group();

    // TODO: Stub
    this.explora_logo = this.game.add.sprite(this.game.width - 10, 10, 'logo');
    this.explora_logo.anchor.setTo(1, 0);

    this.explora_map = this.game.add.sprite(x, y, 'mountain');
    this.explora_map.anchor.setTo(0.5, 0.5);
    this.menuGroup.add(this.explora_map);

    let zen = this.showWelcomeBtn = this.game.add.button(x - 200, y - 150, 'zen_character', this.showWelcome, this);
    zen.anchor.set(0.5, 0.5);
    zen.scale.setTo(0.2, 0.2);
    this.menuGroup.add(zen);

    this.setupWelcome();

    // var style1 = { font: '55px obelixpro', fill: '#fff600', align: 'center'};
    // this.titleText = this.game.add.text(this.game.world.centerX, 204, 'Un par que\nexplora juntos', style1);
    // this.titleText.anchor.setTo(0.5, 0.5);

    // var style2 = { font: '15px didact, sans-serif', fill: '#ffffff', align: 'center'};
    // this.instructionsText = this.game.add.text(this.game.world.centerX, 400, 'La vida nos lleva por muchas aventuras y\n esta la vamos a explorar juntos.', style2);
    // this.instructionsText.anchor.setTo(0.5, 0.5);

    this.gameControls = this.add.group();

    this.controls = this.game.cache.getJSON('worlds_info');
    // this.graphicButtons = this.add.graphics(x, y);
    for(var key in this.controls) {
      var control = this.controls[key];
      // this.graphicButtons.beginFill('#000');
      // this.graphicButtons.drawCircle(control.x, control.y, 25);
      // this.graphicButtons.endFill();

      var button = this.gameControls.create(x + control.x, y + control.y, 'pin');
      button.level = key;
      button.anchor.set(0.5, 1);
      button.scale.setTo(0.16, 0.16);
      button.inputEnabled = true;
      button.input.useHandCursor = true;
      button.name = key;
      button.description = this.addDescription(button, control);
      button.events.onInputDown.add(this.selectLevel, this);

      button.events.onInputOver.add(function(b) {
        if(this.isModal == true) return;
        b.description.alpha = 1;
        // this.game.add.tween(b.description).to({
        //   alpha:1,
        // }, 500, Phaser.Easing.Exponential.Out);
      }, this);
      button.events.onInputOut.add(function(b) {
        b.description.alpha = 0;
        // this.game.add.tween(b.description).to({
        //   alpha:0,
        // }, 500, Phaser.Easing.Exponential.In);
      }, this);
    }

    this.welcomeGroup.alpha = 0;
    let showWelcomeMessage = JSON.parse(localStorage.showWelcomeMessage || "true");
    if(showWelcomeMessage) {
      this.showWelcome();
    }

    // this.menuGroup.forEach(function(b) {
    //   if(b.description) this.game.world.bringToTop(b.description);
    // }, this);

  }

  update() {
    // TODO: Stub
  }

  shutdown() {
    // TODO: Stub
  }
  setupWelcome() {
    let x = this.game.width/2 - 76;
    let y = this.game.height/2;

    let zen = this.zenCharacter = this.game.add.image(x - 280, y, 'zen_character');
    zen.anchor.set(0.5, 0.5);
    zen.scale.setTo(0.9, 0.9);
    this.welcomeGroup.add(zen);

//    <div class="title obelix">Un Par Que Exploran Juntos</div>
//    <p>La vida nos lleva por muchas aventuras y esta la vamos a explorar juntos.</p>




    let title = this.welcomeTitle = this.game.add.text(x + 150, y - 180, 'Un Par Que Exploran Juntos', {
      font: '28px obelixpro',
      wordWrap: true,
      wordWrapWidth: 480,
      align: 'center',
      fill: '#FFF600'
    });
    title.anchor.set(0.5, 0);
    title.lineSpacing = -3;
    this.welcomeGroup.add(title);

    let description = this.welcomeText = this.game.add.text(title.x, title.y + title.height, 'La vida nos lleva por muchas aventuras y esta la vamos a explorar juntos.', {
      font: '14px didact',
      wordWrap: true, 
      wordWrapWidth: 480,
      align: 'left',
      fill: '#EEEEEE'
    });
    description.anchor.set(0.5, 0);
    description.lineSpacing = -5;
    this.welcomeGroup.add(description);

    let button = this.welcomeButton = this.game.add.button(title.x, description.y + description.height + 20, 'play_button', this.hideWelcome, this);
    button.anchor.set(0.5, 0);
    this.welcomeGroup.add(button);


  }
  showWelcome() {
    if(this.isModal) return;
    this.isModal = true;
    this.gameControls.alpha = 0;
    let start = this.game.add.tween(this.menuGroup).to({
      alpha: 0
    }, 500, Phaser.Easing.Exponential.Out);
    let end = this.game.add.tween(this.welcomeGroup).to({
      alpha: 1
    }, 500, Phaser.Easing.Exponential.In);
    start.chain(end);
    start.start();
  }
  hideWelcome() {
    if(!this.isModal) return;
    this.isModal = false;
    localStorage.showWelcomeMessage = JSON.stringify(false);
    let start = this.game.add.tween(this.welcomeGroup).to({
      alpha: 0
    }, 500, Phaser.Easing.Exponential.Out);
    let end = this.game.add.tween(this.menuGroup).to({
      alpha: 1
    }, 500, Phaser.Easing.Exponential.In);
    end.onComplete.add(function() {
      this.gameControls.alpha = 1;
    }, this);
    start.chain(end);
    start.start();
  }
  showMenu() {
    this.welcomeGroup.alpha = 0;
    this.game.add.tween(this.menuGroup).to({
      alpha: 1
    }, 800, Phaser.Easing.Exponential.Out, true);
  }
  addDescription(button, control) {
    let group = this.game.add.group();
    let pos = {x: button.x, y: button.y - button.height - 150}
    let graphics = this.game.add.graphics(pos.x, pos.y);
    // graphics.anchor.set(0.5, 1);
    graphics.beginFill(0xFFFFFF, 1);
    graphics.drawRoundedRect(-50, 0, 100, 150, 20);
    graphics.endFill();

    let title = this.game.add.text(pos.x, pos.y + 4, control.title, {
      font: '9px obelixpro',
      wordWrap: true,
      wordWrapWidth: 92,
      align: 'center',
      fill: '#ef0000'
    });
    title.anchor.set(0.5, 0);
    title.lineSpacing = -4;
    group.title = title;

    let description = this.game.add.text(pos.x, pos.y + 4 + title.height, control.description, {
      font: '10px didact',
      wordWrap: true, 
      wordWrapWidth: 92,
      align: 'left',
      fill: '#666666'
    });
    description.anchor.set(0.5, 0);
    description.lineSpacing = -5;
    group.description = description;

    group.add(graphics);
    group.add(title);
    group.add(description);

    group.alpha = 0;
    // button.addChild(group);

    return group;
    

  }
  selectLevel(button) {
    if(this.isModal) return;
    try {
      var currentStage = localStorage['stage_' + button.level] || '1';
      // this.game.stateTransition.to(button.level + 'Stage'+ currentStage, true, false);

      this.game.state.start(button.level + 'Stage'+ currentStage, true, false);
    } catch(err) {
      console.error(err.message);
    }
  }
  resize() {
    let x = this.game.width/2 - 76;
    let y = this.game.height/2;

    this.explora_logo.x = this.game.width - 10;
    this.explora_map.x = x;
    this.explora_map.y = y;

    // this.graphicButtons.position.x = x;
    // this.graphicButtons.position.y = y;
    this.gameControls.forEach(function(o) {
      o.x = this.controls[o.name].x + x;
      o.y = this.controls[o.name].y + y;
      if(o.description) {
        o.description.setAll('x', this.controls[o.name].x + x);
        o.description.setAll('y', this.controls[o.name].y + y - o.height - 150);
        o.description.title.y += 4;
        o.description.description.y += 4 + o.description.title.height;
      }
    }, this);
    this.showWelcomeBtn.x = x - 180; 
    this.showWelcomeBtn.y = y - 150;
    this.welcomeTitle.x = x + 150;
    this.welcomeTitle.y = y - 180;
    this.welcomeText.x = this.welcomeTitle.x;
    this.welcomeText.y = this.welcomeTitle.y + this.welcomeTitle.height;
    this.welcomeButton.x = this.welcomeText.x;
    this.welcomeButton.y = this.welcomeText.y + this.welcomeText.height + 20;


    this.zenCharacter.x = x - 220;
  }
}
