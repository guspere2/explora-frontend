/*
 * Game state
 * ==========
 *
 * A sample Game state, displaying the Phaser logo.
 */

// import Logo from '../objects/Logo';
import Character from '../objects/Character';

export default class Game extends Phaser.State {

  create() {
    // TODO: Replace this with really cool game code here :)

    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    this.game.physics.arcade.gravity.y = 1000;
    this.game.physics.setBoundsToWorld();

    

    this.demo = this.add.existing(new Character(this.game, 0, 0));
    this.demo.setPhysics();
    
        

    this.resize();
  }
  update() {

  }
  resize() {
    const {centerX: x, centerY: y} = this.world;
    this.demo.x = x;
    this.demo.y = y;

    this.game.world.bounds.setTo(this.world.x, this.world.y, this.game.width, this.game.height);    
    if (this.game.camera.bounds)    {
      //  The Camera can never be smaller than the game size
      this.game.camera.bounds.setTo(this.game.camera.x, this.game.camera.y, this.game.camera.width, this.game.camera.height);
    }
    this.game.physics.setBoundsToWorld();
  }

}
