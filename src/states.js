/*
 * `states` module
 * ===============
 *
 * Declares all present game states.
 * Expose the required game states using this module.
 */

export {default as Boot} from './states/Boot';
export {default as Preload} from './states/Preload';
export {default as Game} from './states/Game';
export {default as Menu} from './states/Menu';
export {default as World1Stage1} from './states/World1Stage1';
export {default as World1Stage2} from './states/World1Stage2';
export {default as World1Stage3} from './states/World1Stage3';
export {default as World1Stage4} from './states/World1Stage4';
export {default as World1Stage5} from './states/World1Stage5';
export {default as World2Stage1} from './states/World2Stage1';
export {default as World2Stage2} from './states/World2Stage2';
export {default as World2Stage3} from './states/World2Stage3';
export {default as World2Stage4} from './states/World2Stage4';
export {default as World2Stage5} from './states/World2Stage5';
export {default as World3Stage1} from './states/World3Stage1';
export {default as World4Stage1} from './states/World4Stage1';
export {default as World5Stage1} from './states/World5Stage1';
export {default as World5Stage2} from './states/World5Stage2';
